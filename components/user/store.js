const Model = require('./model');
const http = require('http');

function addUser(user) {
    const data = JSON.stringify(user);
    return new Promise ((resolve, reject) => {
        const settings = {
            host: 'localhost',
            port: '5000',
            path: '/save',
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }
    
        const req = http.request(settings, res => {

            res.on('data', r => {
                resolve(`${r}`)
            })
        })

        req.on('error', error => reject(error))

        req.write(data);
        req.end()

    })
}

function listUsers() {
    return new Promise ((resolve, reject) => {
        const settings = {
            host: 'localhost',
            port: '5000',
            path: '/',
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }

        const req = http.request(settings, res => {
            console.log(`statusCode: ${res.statusCode}`)
        
            res.on('data', r => {
                resolve(`${r}`)
            })
        })
        
        req.on('error', error => {
            console.error(error)
            reject(error)
        })
        
        req.end()
    })
}

module.exports = {
    add: addUser,
    list: listUsers,
}