const store = require('./store');

function addUser({name, email}) {
    if (!name) {
        return Promise.reject('Invalid name');
    }

    const user = {
        name,
        email
    };
    return store.add(user);
}

function listUsers() {
    return store.list();
}

module.exports = {
    addUser,
    listUsers,
}